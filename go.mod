module recinput

go 1.13

replace github.com/shirou/gopsutil v2.19.6+incompatible => github.com/shirou/gopsutil v2.19.11+incompatible

require (
	github.com/go-vgo/robotgo v0.0.0-20191216133555-c86926da97a5
	github.com/gorilla/websocket v1.4.1 //indirect
	github.com/shirou/gopsutil v2.19.11+incompatible // indirect
	github.com/shirou/w32 v0.0.0-20160930032740-bb4de0191aa4 // indirect
)
