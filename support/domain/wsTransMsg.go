package domain

type ProcType int64

const (
	PROCTYPE_DEFAULT ProcType = iota
	PROCTYPE_KEYBORD
	PROCTYPE_MOUSE
)

type WSRespData struct {
	Group string      `json:"group"`
	Data  interface{} `json:"data"`
}

type InputData struct {
	ProcType  ProcType `json:"proc_type"` //信号类型 键盘 1 鼠标 2
	Key       string   `json:"key"`       //键位
	Direction string   `json:"direction"` //鼠标方向
	Number    string   `json:"number"`    //次数
}

type NewData struct {
	Method string   `json:"method"`
	Args   []string `json:"args"`
}
