package server

import (
	"encoding/json"
	"flag"
	"github.com/gorilla/websocket"
	"log"
	"net/url"
	"recinput/support/domain"
	"strconv"
	"time"
)

//测试端口
//var addr = flag.String("addr", "192.168.10.11:9091", "http service address")
//正式端口
var addr = flag.String("addr", "39.99.132.100", "http service address")

var conn *Connection
var err error

type Connection struct {
	Cli *websocket.Conn
}

func NewConnect() (*Connection, error) {
	c, err := Connect()
	if err != nil {
		log.Println("Connect err:", err)
		return nil, err
	}
	conn := new(Connection)
	conn.Cli = c
	return conn, nil
}

func ServeStart() {
	conn, err = NewConnect()
	if err != nil {
		log.Fatalln("Connect err:", err)
	}
	defer conn.Cli.Close()
FLAG_SUB:
	err = Subscribe()
	if err != nil {
		goto FLAG_SUB
	}
	go HeartBeat()

	select {}
}

func Subscribe() error {
	submsg := map[string]interface{}{
		"action": "subscribe",
		"args":   []string{"input"},
	}
	err := conn.Cli.WriteJSON(submsg)
	if err != nil {
		log.Println("[Subscribe]  WriteJSON err:", err)
		return err
	}
	//基础版本
	//go ReciveMSG(c)

	//船新版本
	go SolveMSG()

	return nil
}

func HeartBeat() {
	ticker := time.NewTicker(time.Second * 10)
	for {
		select {
		case <-ticker.C:
			beatmsg := map[string]interface{}{
				"action": "ping",
			}
			err := conn.Cli.WriteJSON(beatmsg)
			if err != nil {
				log.Println("[HeartBeat]  WriteJSON err:", err)
				//写出错重连
				flag := true
				for flag {
					er := ReConnect()
					if er == nil {
						flag = false
					} else {
						log.Println("[HeartBeat]  ReConnect err:", err)
					}
					time.Sleep(time.Second)
				}
				continue
			}
		}
	}
}

func Connect() (*websocket.Conn, error) {
	flag.Parse()
	u := url.URL{Scheme: "ws", Host: *addr, Path: "/input/realTime"}
	//测试PATH
	//u := url.URL{Scheme: "ws", Host: *addr, Path: "/realTime"}
	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Println("dial:", err)
		return nil, err
	}
	return c, nil
}

//重连
func ReConnect() error {
	log.Println("[ReConnect] EXEC ReConnect...")
	conn, err = NewConnect()
	if err != nil {
		log.Fatalln("Connect err:", err)
	}
	flag := true
	for flag {
		er := Subscribe()
		if er == nil {
			flag = false
		} else {
			log.Println("[HeartBeat]  ReSubscribe err:", err)
		}
		time.Sleep(time.Second)
	}
	return nil
}

func ReciveMSG(cli *websocket.Conn) {
	for {
		_, message, err := cli.ReadMessage()
		if err != nil {
			log.Println("[ReciveMSG] read:", err)
			return
		}
		log.Printf("[ReciveMSG] recv: %s", message)
		var msg domain.WSRespData
		err = json.Unmarshal(message, &msg)
		if err != nil {
			log.Println("[ReciveMSG]  Unmarshal err:", err)
			continue
		}
		switch msg.Group {
		case "System":
			//系统消息，将Data转换为字符串
			data := msg.Data.(string)
			log.Println("[ReciveMSG]  msg:", data)
		case "input":
			b, err := json.Marshal(msg.Data)
			if err != nil {
				log.Println("[ReciveMSG] Marshal(msg.Data) err:", err)
				continue
			}
			var respdata domain.InputData
			err = json.Unmarshal(b, &respdata)
			if err != nil {
				log.Println("[ReciveMSG]  Unmarshal(b,&respdata) err:", err)
				continue
			}
			n, _ := strconv.Atoi(respdata.Number)
			switch respdata.ProcType {
			case domain.PROCTYPE_KEYBORD:
				log.Println("PROCTYPE:", respdata.ProcType, "key:", respdata.Key, "direct:", respdata.Direction, "number:", respdata.Number)
				for i := 1; i <= n; i++ {
					Keyboard(respdata.Key)
				}
			case domain.PROCTYPE_MOUSE:
				log.Println("PROCTYPE:", respdata.ProcType, "key:", respdata.Key, "direct:", respdata.Direction, "number:", respdata.Number)
				for i := 1; i <= n; i++ {
					Mouse(respdata.Key)
				}
			}

		}
	}

}

func SolveMSG() {
	defer func() {
		err := recover()
		if err != nil {
			log.Println("[SolveMSG]  panic:", err)
			SolveMSG()
		}
	}()
	for {
		_, message, err := conn.Cli.ReadMessage()
		if err != nil {
			log.Println("[ReciveMSG] read:", err)
			//读出错重连
			flag := true
			for flag {
				er := ReConnect()
				if er == nil {
					flag = false
				} else {
					log.Println("[SolveMSG]  ReadMessage ReConnect err:", err)
				}
				time.Sleep(time.Second)
			}
			break
		}
		log.Printf("[ReciveMSG] recv: %s", message)
		var msg domain.WSRespData
		err = json.Unmarshal(message, &msg)
		if err != nil {
			log.Println("[ReciveMSG]  Unmarshal err:", err)
			continue
		}
		switch msg.Group {
		case "System":
			//系统消息，将Data转换为字符串
			data := msg.Data.(string)
			log.Println("[ReciveMSG]  msg:", data)
		case "input":
			b, err := json.Marshal(msg.Data)
			if err != nil {
				log.Println("[ReciveMSG] Marshal(msg.Data) err:", err)
				continue
			}
			var respdata domain.NewData
			err = json.Unmarshal(b, &respdata)
			if err != nil {
				log.Println("[ReciveMSG]  Unmarshal(b,&respdata) err:", err)
				continue
			}
			if len(respdata.Args) < 1 {
				log.Println("Args err: have no args!")
				continue
			}
			switch respdata.Method {
			case "SetKeyDelay":
				de, err := strconv.Atoi(respdata.Args[0])
				if err != nil {
					log.Println("[SolveMSG] SetKeyDelay Atoi(respdata.Args[0]) err:", err)
					continue
				}
				SetKeyDelay(de)
			case "KeyTap":
				log.Println("KeyTap args:", respdata.Args)
				if len(respdata.Args) == 1 {
					KeyTap(respdata.Args[0], []string{})
				} else {
					KeyTap(respdata.Args[0], respdata.Args[1:])
				}
			case "KeyToggle":
				if len(respdata.Args) == 1 {
					KeyToggle(respdata.Args[0], []string{})
				} else {
					KeyToggle(respdata.Args[0], respdata.Args[1:])
				}
			case "WriteAll":
				WriteAll(respdata.Args[0])
			case "SetMouseDelay":
				de, err := strconv.Atoi(respdata.Args[0])
				if err != nil {
					log.Println("[SolveMSG] SetMouseDelay Atoi(respdata.Args[0]) err:", err)
					continue
				}
				SetMouseDelay(de)
			case "MoveMouse":
				if len(respdata.Args) < 2 {
					log.Println("Args err: have no enough args!")
					continue
				}
				x, err := strconv.Atoi(respdata.Args[0])
				y, err2 := strconv.Atoi(respdata.Args[1])
				if err != nil || err2 != nil {
					log.Println("[SolveMSG] MoveMouse Atoi(respdata.Args[0]) err:", err)
					continue
				}
				MoveMouse(x, y)
			case "MoveMouseSmooth":
				if len(respdata.Args) < 2 {
					log.Println("Args err: have no enough args!")
					continue
				}
				x, err := strconv.Atoi(respdata.Args[0])
				y, err2 := strconv.Atoi(respdata.Args[1])
				if err != nil || err2 != nil {
					log.Println("[SolveMSG] MoveMouseSmooth Atoi(respdata.Args[0]) err:", err)
					continue
				}
				MoveMouseSmooth(x, y)
			case "MouseClick":
				log.Println("MouseClick  args:", respdata.Args)
				if len(respdata.Args) < 2 {
					log.Println("Args err: have no enough args!")
					continue
				}
				double := false
				de, err := strconv.Atoi(respdata.Args[1])
				if err != nil {
					log.Println("[SolveMSG] MouseClick Atoi(respdata.Args[1]) err:", err)
					continue
				}
				if de == 1 {
					double = true
				}
				MouseClick(respdata.Args[0], double)
			case "MoveClick":
				log.Println("MoveClick  args:", respdata.Args)
				if len(respdata.Args) < 4 {
					log.Println("Args err: have no enough args!")
					continue
				}
				x, err := strconv.Atoi(respdata.Args[0])
				y, err2 := strconv.Atoi(respdata.Args[1])
				if err != nil || err2 != nil {
					log.Println("[SolveMSG] MoveClick Atoi(respdata.Args[0]) err:", err)
					continue
				}
				double := false
				de, err := strconv.Atoi(respdata.Args[3])
				if err != nil {
					log.Println("[SolveMSG] MoveClick Atoi(respdata.Args[0]) err:", err)
					continue
				}
				if de == 1 {
					double = true
				}
				MoveClick(x, y, respdata.Args[2], double)
			case "MouseToggle":
				MouseToggle(respdata.Args[0], respdata.Args[1])
			case "DragMouse":
				if len(respdata.Args) < 3 {
					log.Println("Args err: have no enough args!")
					continue
				}
				x, err := strconv.Atoi(respdata.Args[0])
				y, err2 := strconv.Atoi(respdata.Args[1])
				if err != nil || err2 != nil {
					log.Println("[SolveMSG] DragMouse Atoi(respdata.Args[0]) err:", err)
					continue
				}
				DragMouse(x, y, respdata.Args[2])
			case "ScrollMouse":
				if len(respdata.Args) < 2 {
					log.Println("Args err: have no enough args!")
					continue
				}
				x, err := strconv.Atoi(respdata.Args[0])
				if err != nil {
					log.Println("[SolveMSG] MoveMouse Atoi(respdata.Args[0]) err:", err)
					continue
				}
				ScrollMouse(x, respdata.Args[1])
			}

		}
	}

}
