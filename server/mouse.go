package server

import (
	"github.com/go-vgo/robotgo"
	"log"
	"net/http"
)

func Mouse(direct string) {
	robotgo.Click(direct, true)
}

func MoveAndClick(w http.ResponseWriter, r *http.Request) {
	robotgo.ScrollMouse(10, "up")
	robotgo.MoveMouseSmooth(300, 300, 1.0, 100.0)
	robotgo.MouseClick("left", true)

}

//设置鼠标延迟,参数：（延迟时间）
func SetMouseDelay(delay int) {
	robotgo.SetMouseDelay(delay)
}

//鼠标移动，参数：(横坐标x,纵坐标y)
func MoveMouse(x, y int) {
	robotgo.MoveMouse(x, y)
}

//平滑的将鼠标移动到指定坐标（横坐标x,纵坐标y）
func MoveMouseSmooth(x, y int) {
	robotgo.MoveMouseSmooth(x, y)
}

//点击鼠标，参数：（左left/中center/右right键，是1/否0双击）
func MouseClick(button string, double bool) {
	log.Println("button:", button, " double:", double)
	robotgo.MouseClick(button, double)
}

//MoveMouse + MouseClick, 参数（横坐标x,纵坐标y，左left/中center/右right键，是1/否0双击）
func MoveClick(x, y int, button string, double bool) {
	log.Println("x:", x, "y:", y, "button:", button, " double:", double)
	robotgo.MoveClick(x, y, button, double)
}

//按住鼠标，参数：（下down/上up,左left/中center/右right键）
func MouseToggle(togKey, button string) {
	robotgo.MouseToggle(togKey, button)
}

//拖动鼠标，参数：（横坐标x,纵坐标y，左left/中center/右right键）
func DragMouse(x, y int, button string) {
	robotgo.DragMouse(x, y, button)
}

//滚动鼠标，参数：（滚动多少，滚动方向）
func ScrollMouse(x int, direction string) {
	robotgo.ScrollMouse(x, direction)
}
