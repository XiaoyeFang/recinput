package server

import (
	"github.com/go-vgo/robotgo"
	"log"
	"net/http"
	"strings"
	"time"
)

func Rewrite(w http.ResponseWriter, r *http.Request) {
	text := r.FormValue("text")
	robotgo.TypeStr(text)
	robotgo.KeyTap("enter")

	//robotgo.Sleep(3)
	//robotgo.KeyTap("i", "command", "tab")
}

func Keyboard(key string) {
	key = strings.ToLower(key)
	re := robotgo.KeyTap(key)
	log.Println(re)
	//w.WriteHeader(200)
	//fmt.Fprintln(w, result)

}

func Ctrl(w http.ResponseWriter, r *http.Request) {
	robotgo.KeyTap("ctrl")
}

func Space(w http.ResponseWriter, r *http.Request) {
	robotgo.KeyTap("space")
}

//延迟操作
func SetKeyDelay(delay int) {
	robotgo.SetKeyDelay(delay)
}

//键盘操作
func KeyTap(key string, modifier []string) {
	key = strings.ToLower(key)
	log.Println("key:", key, "modifier:", modifier)
	if len(modifier) == 0 {
		//re := robotgo.KeyTap(key)
		re := robotgo.KeyToggle(key, "down")
		log.Println("KeyTap down return:", re)
		time.Sleep(time.Millisecond * 30)
		re = robotgo.KeyToggle(key, "up")
		log.Println("KeyTap up return:", re)
	} else {
		re := robotgo.KeyTap(key, modifier)
		log.Println("KeyTap return:", re)
	}
}

//键盘切换, 按住或释放一个键位
func KeyToggle(key string, args []string) {
	robotgo.KeyToggle(key, args...)
}

//键盘输入文本
func WriteAll(text string) {
	log.Println("EXEC WriteAll text： ", text)
	robotgo.WriteAll(text)

}
